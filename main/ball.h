#pragma once
#include "raylib.h"

struct BALL
{
	float radius;
	Vector2 position;	
	Color color;
};
